package com.blooms.twidapp.base

import android.app.Activity
import android.view.View

interface BasePresenter  {

    abstract fun setView(view: View)

    abstract fun setActivity(activity: Activity)
}