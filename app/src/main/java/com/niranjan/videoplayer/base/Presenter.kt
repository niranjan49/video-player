package com.niranjan.videoplayer.base

import com.niranjan.videoplayer.Cache.AppCache

open class Presenter {

    fun getAppCache(): AppCache {
        return AppCache.getCache()
    }
}