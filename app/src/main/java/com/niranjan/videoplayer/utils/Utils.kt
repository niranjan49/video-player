package com.blooms.twidapp.utils

import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Geocoder
import android.location.LocationManager
import android.provider.Settings
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.DisplayImageOptions.*
import com.nostra13.universalimageloader.core.ImageLoader
import java.util.*

class Utils {

    companion object {

        fun displayImage(context: Context, url: String?, view: ImageView) {
            if (TextUtils.isEmpty(url)) {
                view.visibility=View.INVISIBLE
                return
            }
            val imageLoader = ImageLoader.getInstance()
            val options = Builder().cacheInMemory(true)
                    .cacheOnDisk(true).resetViewBeforeLoading(true)
                    .build()

            imageLoader.displayImage(url, view, options)
        }

    }

}