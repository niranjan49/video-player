package com.blooms.twidapp.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.niranjan.videoplayer.utils.CustomProgressDialog


class DialogUtil {
    companion object {
        internal var m_cProgressBar: CustomProgressDialog? = null

        fun displayProgress(pContext: Activity) {
            displayProgress(pContext, "Please wait..")
        }

        fun displayProgress(pContext: Activity?, message: String) {

            if (null == m_cProgressBar) {
                if (pContext == null) return
                if (pContext.isFinishing)
                    return
                m_cProgressBar = CustomProgressDialog(pContext)
                //  m_cProgressBar.getWindow().getAttributes().windowAnimations = R.style.ProgressAnimation;
                m_cProgressBar!!.setCancelable(false)
                m_cProgressBar!!.show()
            }
        }

        fun stopProgressDisplay() {
            if (null != m_cProgressBar && m_cProgressBar!!.isShowing()) {
                try {
                    m_cProgressBar!!.dismiss()
                } catch (e: Exception) {

                }

            }
            m_cProgressBar = null
        }
    }
}