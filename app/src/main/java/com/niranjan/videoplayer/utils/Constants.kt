package com.blooms.twidapp.utils

class Constants {
    companion object {

        const val BASE_URL = "https://interview-e18de.firebaseio.com/"
        const val URL_DATA = "media.json"
        const val QUERY_PARAMETER = "pretty"
        const val FRAGMENT_LOGIN = 1
        const val FRAGMENT_VIDEO_DETAILS = 2
        const val FRAGMENT_HOME = 3

    }
}