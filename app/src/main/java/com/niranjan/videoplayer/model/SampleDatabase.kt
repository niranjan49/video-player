package com.niranjan.videoplayer.model

import android.arch.lifecycle.MutableLiveData
import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = arrayOf(VideoData::class), version = 1)
abstract class SampleDatabase : RoomDatabase() {
    private val mIsDatabaseCreated = MutableLiveData<Boolean>()
    abstract fun videoListDao(): VideoListDao

    companion object {
        val DATABASE_NAME = "videoList"
    }
}