package com.niranjan.videoplayer.model

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update

@Dao
interface VideoListDao {
    @Query("SELECT COUNT(*) FROM " + VideoData.TABLE_NAME)
    abstract fun count(): Int

    @Query("SELECT COUNT(*) FROM " + VideoData.TABLE_NAME + " where " + VideoData.COLUMN_URL + " =:url")
    abstract fun countByUrl(url: String): Int

    @Query("SELECT " + VideoData.COLUMN_POSITION + " FROM " + VideoData.TABLE_NAME + " where " + VideoData.COLUMN_URL + " =:url")
    abstract fun getPosition(url: String): Long

    @Insert
    abstract fun insert(videoList: VideoData): Long

    @Update
    abstract fun update(videoList: VideoData): Int
}