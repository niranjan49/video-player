package com.niranjan.videoplayer.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = VideoData.TABLE_NAME)
class VideoData{

    companion object {

        const val TABLE_NAME= "video"
        const val COLUMN_ID = "id"
        const val COLUMN_DESCRIPTION = "description"
        const val COLUMN_THUMB = "thumb"
        const val COLUMN_TITLE = "title"
        const val COLUMN_URL = "url"
        const val COLUMN_POSITION = "playbackPosition"

    }

    var description :String ?=null
    @PrimaryKey(autoGenerate = true)
    var id:Int?=0
    var thumb :String?=null
    var title:String?=null
    var url:String?=null
    var playbackPosition: Long = 0

}
