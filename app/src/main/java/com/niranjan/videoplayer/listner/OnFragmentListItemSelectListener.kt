package com.blooms.twidapp.listner

import android.view.View

interface OnFragmentListItemSelectListener {
    abstract fun onListItemSelected(itemId: Int, data: Any)
    abstract fun onListItemLongClicked(itemId: Int, data: Any, view: View)
}