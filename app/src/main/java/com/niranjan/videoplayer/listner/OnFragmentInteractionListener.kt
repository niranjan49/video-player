package com.blooms.twidapp.listner

interface OnFragmentInteractionListener {

    abstract fun onFragmentInteraction(fragmentId: Int, data: Any?)
}