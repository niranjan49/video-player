package com.niranjan.videoplayer

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import android.support.multidex.MultiDex
import com.google.firebase.FirebaseApp
import com.niranjan.videoplayer.model.SampleDatabase
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.ImageScaleType
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer

class VideoPlayerApplication : Application() {

    companion object {
        private var instance: VideoPlayerApplication? = null

        fun getInstance(): VideoPlayerApplication {
            return instance!!
        }
    }

    var sampleDatabase: SampleDatabase? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
        FirebaseApp.initializeApp(this@VideoPlayerApplication)

        val defaultOptions = DisplayImageOptions.Builder()
            .cacheOnDisk(true).cacheInMemory(true)
            .imageScaleType(ImageScaleType.EXACTLY)
            .displayer(FadeInBitmapDisplayer(300)).build()

        val config = ImageLoaderConfiguration.Builder(
            getApplicationContext()
        )
            .memoryCacheExtraOptions(480, 800)
            .defaultDisplayImageOptions(defaultOptions)
            .memoryCache(WeakMemoryCache())
            .diskCacheSize(60 * 1024 * 1024).build()

        ImageLoader.getInstance().init(config)
        sampleDatabase = Room.databaseBuilder(this@VideoPlayerApplication, SampleDatabase::class.java, "videoList")
            .allowMainThreadQueries().build()

    }

    fun initDB(): SampleDatabase {

        return sampleDatabase!!
    }

    override fun onTerminate() {
        super.onTerminate()
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onLowMemory() {
        super.onLowMemory()

    }

}