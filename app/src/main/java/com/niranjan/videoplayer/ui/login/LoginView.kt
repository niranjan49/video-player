package com.niranjan.videoplayer.ui.login

import android.app.Activity
import com.blooms.twidapp.base.BasePresenter
import com.blooms.twidapp.base.BaseView
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.niranjan.videoplayer.ui.home.HomeView

interface LoginView {

    interface View : BaseView<HomeView.Presenter>

    interface Presenter : BasePresenter {
        fun validateAccount(account: GoogleSignInAccount)
    }

}