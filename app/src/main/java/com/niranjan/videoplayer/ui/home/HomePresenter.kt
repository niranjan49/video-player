package com.niranjan.videoplayer.ui.home

import android.app.Activity
import android.util.Log
import android.view.View
import com.blooms.twidapp.listner.OnFragmentListItemSelectListener
import com.blooms.twidapp.utils.Constants
import com.blooms.twidapp.utils.DialogUtil
import com.google.firebase.database.*
import com.niranjan.videoplayer.R
import com.niranjan.videoplayer.base.Presenter
import com.niranjan.videoplayer.model.VideoData
import com.niranjan.videoplayer.network.EventListner
import com.niranjan.videoplayer.network.TransportManager
import java.util.*

class HomePresenter(homeView: HomeView.View) : HomeView.Presenter, Presenter(), EventListner,OnFragmentListItemSelectListener {
    var view: HomeView.View? = homeView
    var rootView: View? = null

    private fun initPresenter(rootView: View) {
        view?.initViews(rootView)
    }

    override fun getData(context: Activity) {
        val videoLists = getAppCache().videoLists

        if (videoLists != null && videoLists!!.size > 0) {
            view?.setAdapterData(videoLists)
        } else {
            DialogUtil.displayProgress(context)
            TransportManager.getInstance(this).getVideoList(context)
        }
    }

    override fun onSuccessResponse(reqType: Int, data: Any) {
        DialogUtil.stopProgressDisplay()
        val dataList = data as ArrayList<VideoData>

        getAppCache().videoLists = dataList as ArrayList<VideoData>
        view?.setAdapterData(dataList)
    }

    override fun onFailureResponse(reqType: Int, data: Any) {
        DialogUtil.stopProgressDisplay()
    }
    override fun getListener(): OnFragmentListItemSelectListener? {
        return this
    }

    override fun onListItemSelected(itemId: Int, data: Any) {
        when (itemId) {
            R.id.layout_item -> {
                val list = data as VideoData
                view?.gotoScreen(Constants.FRAGMENT_VIDEO_DETAILS, list)
            }
        }
    }

    override fun onListItemLongClicked(itemId: Int, data: Any, view: View) {
    }

    override fun setView(view: View) {
        rootView = view
        initPresenter(rootView!!)
    }

    override fun setActivity(activity: Activity) {
    }
}