package com.niranjan.videoplayer.ui.videodetails

import android.app.Activity
import android.net.Uri
import android.view.View
import com.blooms.twidapp.listner.OnFragmentListItemSelectListener
import com.blooms.twidapp.utils.DialogUtil
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerControlView
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.niranjan.videoplayer.Cache.AppCache
import com.niranjan.videoplayer.R
import com.niranjan.videoplayer.VideoPlayerApplication
import com.niranjan.videoplayer.base.Presenter
import com.niranjan.videoplayer.model.VideoData
import com.niranjan.videoplayer.network.EventListner
import com.niranjan.videoplayer.network.TransportManager


class VideoPresenter(view: VideoView.View, context: Activity) : VideoView.Presenter, Presenter(),
    EventListner, OnFragmentListItemSelectListener, PlayerControlView.VisibilityListener, PlaybackPreparer {

    var context: Activity? = context
    var selectedVideodata: VideoData? = null
    var player1: SimpleExoPlayer? = null

    internal var playbackPos: Long? = 0
    internal var currentWindow: Int? = 0
    internal var playWhenReady: Boolean = false

    override fun onVisibilityChange(visibility: Int) {

    }

    override fun preparePlayback() {
    }

    override fun onSuccessResponse(reqType: Int, data: Any) {

        val dataList = data as java.util.ArrayList<VideoData>
        getAppCache().videoLists = dataList as java.util.ArrayList<VideoData>

        view?.setAdapterData(dataList)
    }

    override fun onFailureResponse(reqType: Int, data: Any) {

    }

    override fun getVideoData(data: VideoData) {
        selectedVideodata = data

    }

    override fun onListItemSelected(itemId: Int, data: Any) {
        when (itemId) {
            R.id.layout_item -> {
                releasePlayer()
                selectedVideodata = data as VideoData
                view?.initializeRPlayer(selectedVideodata!!)

                getRelatedVideos(selectedVideodata!!, context as Activity)
                player1!!.playWhenReady = true
            }
        }
    }

    override fun onListItemLongClicked(itemId: Int, data: Any, view: View) {
    }

    override fun getRelatedVideos(video: VideoData, context: Activity) {
        val videoLists = AppCache.getCache().videoLists
        if (videoLists != null) {

            val relatedList = ArrayList<VideoData>(videoLists!!)
            relatedList.remove(video)
            view?.setAdapterData(relatedList)
        } else {
            DialogUtil.displayProgress(context)
            TransportManager.getInstance(this).getVideoList(context)
        }
    }

    override fun getListener(): OnFragmentListItemSelectListener? {
        return this
    }

    override fun updateVideoPosition(video: VideoData) {
        val count = VideoPlayerApplication.getInstance().initDB().videoListDao().countByUrl(video.url!!)
        if (count == 1) {
            VideoPlayerApplication.getInstance().initDB().videoListDao().update(video)
        } else {
            VideoPlayerApplication.getInstance().initDB().videoListDao().insert(video)
        }
    }

    override fun setView(view1: View) {
        rootView = view1
        initPresenter(rootView!!)
    }

    override fun setActivity(activity: Activity) {
    }

    var view: VideoView.View? = view
    var rootView: View? = null

    private fun initPresenter(rootView: View) {
        view?.initViews(rootView)
    }

    override fun releasePlayer() {
        if (player1 != null) {
            playbackPos = player1?.getCurrentPosition()!!
            selectedVideodata?.playbackPosition = playbackPos as Long
            updateVideoPosition(this!!.selectedVideodata!!)
            currentWindow = player1!!.getCurrentWindowIndex()
            playWhenReady = player1!!.getPlayWhenReady()
            player1?.release()
            player1 = null

        }
    }

    override fun initializePlayer(video_view: PlayerView?) {
        player1 = ExoPlayerFactory.newSimpleInstance(
            DefaultRenderersFactory(context),
            DefaultTrackSelector(), DefaultLoadControl()
        )

        video_view?.setPlayer(player1)
        video_view?.setControllerVisibilityListener(this)
        video_view?.requestFocus()
        video_view?.setPlaybackPreparer(this)

        val uri = Uri.parse(selectedVideodata?.url)
        val mediaSource = buildMediaSource(uri)
        player1!!.prepare(mediaSource, true, false)
        player1!!.seekTo(VideoPlayerApplication.getInstance().initDB().videoListDao().getPosition(selectedVideodata?.url!!))


        player1?.addListener(object : Player.DefaultEventListener() {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                if (playWhenReady && playbackState == Player.STATE_READY) {
                    view?.checkPlayReady(true)
                } else if (playWhenReady) {
                } else {
                    view?.checkPlayReady(false)
                }
            }
        })
    }

    override fun playVideo() {
        player1!!.playWhenReady = true
    }

    private fun buildMediaSource(uri: Uri): MediaSource {
        return ExtractorMediaSource.Factory(
            DefaultHttpDataSourceFactory("exoplayer-codelab")
        ).createMediaSource(uri)
    }
}