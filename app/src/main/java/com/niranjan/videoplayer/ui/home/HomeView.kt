package com.niranjan.videoplayer.ui.home

import android.app.Activity
import com.blooms.twidapp.base.BasePresenter
import com.blooms.twidapp.base.BaseView
import com.blooms.twidapp.listner.OnFragmentListItemSelectListener
import com.niranjan.videoplayer.model.VideoData
import java.util.ArrayList

interface HomeView  {

    interface View : BaseView<Presenter> {
        fun setAdapterData(videoLists: ArrayList<VideoData>)
        fun gotoScreen(fragmentId: Int, data: Any)
    }

    interface Presenter : BasePresenter {
        fun getData(context: Activity)
        fun getListener(): OnFragmentListItemSelectListener?
    }
}