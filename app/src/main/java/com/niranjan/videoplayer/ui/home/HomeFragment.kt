package com.niranjan.videoplayer.ui.home

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.blooms.twidapp.utils.DialogUtil
import com.niranjan.videoplayer.R
import com.niranjan.videoplayer.base.BaseFragment
import com.niranjan.videoplayer.model.VideoData
import com.niranjan.videoplayer.ui.home.adapters.VideoListAdapter
import java.util.*

class HomeFragment : BaseFragment(), HomeView.View {

      var presenter: HomeView.Presenter? = null
    var videoListAdapter: VideoListAdapter? = null
    var video_list: RecyclerView? = null

    companion object {
        fun getInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onResume() {
        super.onResume()

        presenter?.getData(context as Activity)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        presenter = HomePresenter(this)
        presenter?.setView(rootView)

        return rootView
    }

    override fun gotoScreen(fragmentId: Int, data: Any) {
        mListener?.onFragmentInteraction(fragmentId, data)
    }
    override fun setAdapterData(videoLists: ArrayList<VideoData>) {
        DialogUtil.stopProgressDisplay()
        if (videoLists != null) {
            videoListAdapter = VideoListAdapter(videoLists, getmActivity() as Activity)
            videoListAdapter!!.setListner(presenter?.getListener()!!)
            video_list!!.adapter = videoListAdapter
        }
    }

    override fun initViews(view: View) {
        video_list = view.findViewById(R.id.video_list)
        video_list?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    override fun successResult() {
    }

    override fun failureResult() {
    }
}