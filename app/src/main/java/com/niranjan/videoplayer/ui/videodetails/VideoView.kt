package com.niranjan.videoplayer.ui.videodetails

import android.app.Activity
import com.blooms.twidapp.base.BasePresenter
import com.blooms.twidapp.base.BaseView
import com.blooms.twidapp.listner.OnFragmentListItemSelectListener
import com.google.android.exoplayer2.ui.PlayerView
import com.niranjan.videoplayer.model.VideoData
import java.util.*

interface VideoView {

    interface View : BaseView<VideoView.Presenter> {
        fun setAdapterData(videoLists: ArrayList<VideoData>)
        fun gotoScreen(fragmentId: Int, data: Any)
        fun initializeRPlayer(videoData: VideoData)
        fun checkPlayReady(isPlaying: Boolean)
    }

    interface Presenter : BasePresenter {
        fun getRelatedVideos(video: VideoData, context: Activity)
        fun getListener(): OnFragmentListItemSelectListener?
        fun updateVideoPosition(list: VideoData)
        fun initializePlayer(video_view: PlayerView?)
        fun releasePlayer()
        fun getVideoData(videoData: VideoData)
        fun playVideo()
    }
}