package com.niranjan.videoplayer.ui.login

import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Intent
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.util.Log
import android.view.View
import com.blooms.twidapp.utils.DialogUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.niranjan.videoplayer.R

class LoginPresenter(loginView: LoginView.View, context: Activity) : LoginView.Presenter {

    private var mAuth: FirebaseAuth? = null

    var view: LoginView.View? = loginView
    var context: Activity? = context
    var rootView: View? = null

    override fun validateAccount(acct: GoogleSignInAccount) {
        DialogUtil.displayProgress(context as Activity)

        val credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null)
        mAuth?.signInWithCredential(credential)
            ?.addOnCompleteListener(context as Activity,
                OnCompleteListener<AuthResult> { task ->
                    if (task.isSuccessful) {
                        view?.successResult()
                    } else {
                        view?.failureResult()
                    }
                    DialogUtil.stopProgressDisplay()
                })
    }

    private fun initPresenter(rootView: View) {
        view?.initViews(rootView)
    }

    override fun setView(view: View) {
        rootView = view
        initPresenter(rootView!!)
    }

    override fun setActivity(activity: Activity) {
        mAuth = FirebaseAuth.getInstance()
    }
}