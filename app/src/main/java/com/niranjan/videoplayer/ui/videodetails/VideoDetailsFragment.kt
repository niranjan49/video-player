package com.niranjan.videoplayer.ui.videodetails

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.blooms.twidapp.utils.DialogUtil
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.ui.PlayerView
import com.niranjan.videoplayer.R
import com.niranjan.videoplayer.base.BaseFragment
import com.niranjan.videoplayer.model.VideoData
import com.niranjan.videoplayer.ui.videodetails.adapters.RelatedVideosListAdapter


class VideoDetailsFragment : BaseFragment(), VideoView.View, View.OnClickListener {
    var presenter: VideoView.Presenter? = null

    var video_view: PlayerView? = null
    var text_title: TextView? = null
    var text_desc: TextView? = null
    var related_list: RecyclerView? = null
    var videoData: VideoData? = null
    var relatedVideosListAdapter: RelatedVideosListAdapter? = null
    var player: SimpleExoPlayer? = null
    var button_play: Button? = null

    companion object {
        fun getInstace(): VideoDetailsFragment {
            return VideoDetailsFragment()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter?.getRelatedVideos(videoData!!, getmActivity())
        presenter?.getVideoData(videoData!!)
        presenter?.initializePlayer(video_view)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_video_details, container, false)

        presenter = VideoPresenter(this, context as Activity)
        presenter?.setView(rootView)

        return rootView
    }

    override fun initializeRPlayer(videoData: VideoData) {
        text_title?.setText(videoData?.title)
        text_desc?.setText(videoData?.description)
        presenter?.getVideoData(videoData!!)
        presenter?.initializePlayer(video_view)
    }

    override fun setAdapterData(videoLists: ArrayList<VideoData>) {
        DialogUtil.stopProgressDisplay()
        if (videoLists != null) {
            relatedVideosListAdapter = RelatedVideosListAdapter(videoLists, getmActivity() as Activity)
            relatedVideosListAdapter!!.setListner(presenter?.getListener()!!)
            related_list!!.adapter = relatedVideosListAdapter
        }
    }

    override fun gotoScreen(fragmentId: Int, data: Any) {
    }

    override fun checkPlayReady(isPlaying:Boolean) {
        if (isPlaying == true) {
            button_play!!.visibility = View.GONE
        } else {
            button_play!!.visibility = View.VISIBLE
        }
    }

    override fun initViews(rootView: View) {
        text_title = rootView.findViewById(R.id.text_title)
        text_desc = rootView.findViewById(R.id.text_desc)
        video_view = rootView.findViewById(R.id.player_view)
        button_play = rootView.findViewById(R.id.button_play)
        button_play?.setOnClickListener(this)
        related_list = rootView.findViewById(R.id.related_list)
        related_list?.setLayoutManager(LinearLayoutManager(getmActivity(), LinearLayoutManager.VERTICAL, false))

        text_title?.setText(videoData?.title)
        text_desc?.setText(videoData?.description)
        presenter?.getVideoData(videoData!!)
        presenter?.initializePlayer(video_view)
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.button_play -> {
                presenter?.playVideo()
                button_play!!.visibility = View.GONE
            }
        }
    }

    override fun successResult() {
    }

    override fun failureResult() {
    }

    fun setVideoDetails(details: VideoData) {
        this.videoData = details
    }

    override fun onPause() {
        super.onPause()
        presenter?.releasePlayer()
    }
}
