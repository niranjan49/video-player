package com.niranjan.videoplayer.ui.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.blooms.twidapp.utils.Constants
import com.blooms.twidapp.utils.DialogUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.niranjan.videoplayer.R
import com.niranjan.videoplayer.base.BaseFragment


class LoginFragment : BaseFragment(), LoginView.View, View.OnClickListener {

    var mGoogleSignInClient: GoogleSignInClient? = null

    var presenter: LoginView.Presenter? = null
    var activity: Activity? = null
    internal val RC_SIGN_IN = 9001
    var login: Button? = null

    private fun signIn() {
        DialogUtil.displayProgress(context as Activity)
        val signInIntent = mGoogleSignInClient?.getSignInIntent()
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.google -> signIn()
        }
    }

    companion object {
        fun getInstance(): LoginFragment {
            return LoginFragment()
        }
    }

    override fun initViews(view: View) {
        login = view?.findViewById(R.id.google)
        login?.setOnClickListener(this)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(context as Activity, gso)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.activity_login, container, false);

        presenter = LoginPresenter(this, context as Activity)
        presenter?.setView(rootView)
        presenter?.setActivity(context as Activity)
        return rootView
    }

    override fun successResult() {
        DialogUtil.stopProgressDisplay()
        Toast.makeText(context, getString(R.string.login_success), Toast.LENGTH_SHORT).show()
        mListener?.onFragmentInteraction(Constants.FRAGMENT_HOME, null)
    }

    override fun failureResult() {
        DialogUtil.stopProgressDisplay()
        Toast.makeText(context, getString(R.string.lodin_failed), Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        DialogUtil.stopProgressDisplay()
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                presenter?.validateAccount(account!!)
            } catch (e: ApiException) {
            }

        }
    }
}