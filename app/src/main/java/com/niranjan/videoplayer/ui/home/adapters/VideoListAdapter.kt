package com.niranjan.videoplayer.ui.home.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.blooms.twidapp.listner.OnFragmentListItemSelectListener
import com.blooms.twidapp.utils.Utils
import com.niranjan.videoplayer.R
import com.niranjan.videoplayer.model.VideoData

class VideoListAdapter(items: ArrayList<VideoData>, activity: Context) :
    RecyclerView.Adapter<VideoListAdapter.ViewHolder>() {
    var context: Context? = activity
    var itemList: ArrayList<VideoData>? = items

    var mListner: OnFragmentListItemSelectListener? = null

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): VideoListAdapter.ViewHolder {
        val row = LayoutInflater.from(p0.getContext()).inflate(R.layout.video_list_item, p0, false)
        return ViewHolder(row);
    }

    fun setListner(listener: OnFragmentListItemSelectListener) {
        this.mListner = listener
    }

    override fun getItemCount(): Int {
        return if (itemList == null) 0 else itemList!!.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        val item = itemList!![p1]
        p0.title.setText(item.title)
        p0.description.setText(item.description)
        Utils.displayImage(context!!, item.thumb, p0.image_thumb)

        p0.container!!.setOnClickListener {
            mListner!!.onListItemSelected(R.id.layout_item, item) // <- item instance of ItemObject
        }
    }

    inner class ViewHolder(private val mView: View) : RecyclerView.ViewHolder(mView) {

        internal var viewItemID: Int = 0
        internal var title: TextView
        internal var image_thumb: ImageView
        internal var description: TextView
        internal var container: View? = null

        init {
            container = itemView.findViewById(R.id.layout_item)
            title = mView.findViewById(R.id.text_title)
            description = mView.findViewById(R.id.text_descrition)
            image_thumb = mView.findViewById(R.id.image_thumb)
        }
    }
}