package com.niranjan.videoplayer.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import com.blooms.twidapp.listner.OnFragmentInteractionListener
import com.blooms.twidapp.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.niranjan.videoplayer.R
import com.niranjan.videoplayer.base.BaseActivity
import com.niranjan.videoplayer.model.VideoData
import com.niranjan.videoplayer.ui.home.HomeFragment
import com.niranjan.videoplayer.ui.login.LoginFragment
import com.niranjan.videoplayer.ui.videodetails.VideoDetailsFragment

class MainActivity : BaseActivity(), OnFragmentInteractionListener {

    internal var currentFragment: Int = 0
    internal var previousFragment: Fragment? = null
    internal var nextFragment: Fragment? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val mAuth = FirebaseAuth.getInstance()
        val currentUser = mAuth.currentUser
        if (currentUser == null)
            onFragmentInteraction(Constants.FRAGMENT_LOGIN, null);
        else
            onFragmentInteraction(Constants.FRAGMENT_HOME, null)
    }

    override fun onFragmentInteraction(fragmentId: Int, data: Any?) {
        currentFragment = fragmentId
        val fragmentTag = fragmentId.toString()

        when (fragmentId) {

            Constants.FRAGMENT_LOGIN -> {
                navigateScreen(fragmentTag, LoginFragment.getInstance())
            }
            Constants.FRAGMENT_HOME -> {
                navigateScreen(fragmentTag, HomeFragment.getInstance())
            }

            Constants.FRAGMENT_VIDEO_DETAILS -> {
                val details = VideoDetailsFragment.getInstace()
                details.setVideoDetails(data as VideoData)
                navigateScreen(fragmentTag, details)
            }
        }
    }

    private fun navigateScreen(fragmentTag: String, currentFragment: Fragment) {
        navigateScreen(fragmentTag, currentFragment, true)
    }

    private fun navigateScreen(fragmentTag: String, currentFragment: Fragment, addToBackStack: Boolean) {
        nextFragment = currentFragment
        val fragmentManager = getSupportFragmentManager()
        val transaction = fragmentManager.beginTransaction()
        transaction.addToBackStack(fragmentTag)

        transaction.replace(R.id.fragment_main, currentFragment, fragmentTag).commit()
        previousFragment = nextFragment
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count <= 1) {
            closeApp()
        } else {
            super.onBackPressed()
        }
    }


    fun closeApp() {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage(getString(R.string.exit_message))

        alertDialogBuilder.setPositiveButton("Yes") { arg0, arg1 -> finish() }
        alertDialogBuilder.setNegativeButton("no") { dialog, which -> dialog.dismiss() }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }
}
