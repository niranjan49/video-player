package com.niranjan.videoplayer.network

import com.blooms.twidapp.utils.Constants
import com.niranjan.videoplayer.model.VideoData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface ApiServices {

    companion object {
        val REQUEST_VIDEOS_DATA = 1
    }

    @GET(Constants.URL_DATA)
    abstract fun getVideosList(@Query(Constants.QUERY_PARAMETER) pretty: String): Call<ArrayList<VideoData>>
}