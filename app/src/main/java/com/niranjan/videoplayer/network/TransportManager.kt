package com.niranjan.videoplayer.network

import android.content.Context
import android.net.ConnectivityManager
import com.blooms.twidapp.utils.Constants
import com.blooms.twidapp.utils.DialogUtil
import com.google.gson.GsonBuilder
import com.niranjan.videoplayer.model.VideoData
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

class TransportManager {


    private var apiServices: ApiServices? = null
    internal lateinit var listener: EventListner

    companion object {
        var manager: TransportManager? = null
        fun getInstance(conlistener: EventListner): TransportManager {
            if (manager == null)
                manager = TransportManager()
            manager!!.setListener(conlistener)
            return manager as TransportManager
        }
    }

    fun setListener(listener: EventListner) {
        this.listener = listener
    }

    fun getAPIService(): ApiServices? {
        val gson = GsonBuilder()
            .setLenient()
            .create()

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val builder = OkHttpClient.Builder()
        builder.interceptors().add(interceptor)
        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(90, TimeUnit.SECONDS)

        val client = builder.build()
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()
        apiServices = retrofit.create(ApiServices::class.java)
        return apiServices
    }

    fun getVideoList(context: Context) = if (isConnectionAvailable(context)) {
        getAPIService()!!.getVideosList(Constants.QUERY_PARAMETER).enqueue(object : Callback<ArrayList<VideoData>> {
            override fun onResponse(call: Call<ArrayList<VideoData>>, res: Response<ArrayList<VideoData>>) {
                if (res.isSuccessful) {
                    listener.onSuccessResponse(ApiServices.REQUEST_VIDEOS_DATA, res.body()!!)
                } else {
                    processResponse(res, ApiServices.REQUEST_VIDEOS_DATA)
                }
            }

            override fun onFailure(call: Call<ArrayList<VideoData>>, arg0: Throwable) {
                listener.onFailureResponse(ApiServices.REQUEST_VIDEOS_DATA, arg0.localizedMessage)
            }
        })
    } else {
        listener.onFailureResponse(ApiServices.REQUEST_VIDEOS_DATA, "NO_INTERNET")
    }

    fun isConnectionAvailable(context: Context?): Boolean {
        if (context == null) return false
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    fun processResponse(res: Response<*>, reqType: Int) {
        DialogUtil.stopProgressDisplay()
    }
}