package com.niranjan.videoplayer.Cache

import com.niranjan.videoplayer.model.VideoData
import java.util.ArrayList

class AppCache{

    companion object {
        private var cache: AppCache? = null

        fun getCache(): AppCache {
            if (cache == null) cache = AppCache()
            return cache as AppCache
        }
    }
    var videoLists: ArrayList<VideoData>? = null
}